package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Controller {
    TuringCore tm;

    public Controller(){
        tm = new TuringCore("hg");
    }

    @FXML
    Button def_btn;

    @FXML
    ListView<String> listView;

    @FXML
    public void default_action_btn(){
        System.out.println("Kasia");
        tm.nextInput();
        System.out.println("Current state is: " + tm.getState());

        def_btn.setText("Kasia");
        //def_btn.setDisable(true);
        List<String> list = new ArrayList();
        list.add("Paweł");
        list.add("Kasia");
        ObservableList<String> list2 = FXCollections.observableList(list);
        listView.setItems(list2);
    }
}
